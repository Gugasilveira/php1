<!DOCTYPE html>
<html>
<head>
	<title> Site do Brafé</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php wp_head() ?>
</head>
<body>
	<header class="menu">
		<div class="container">
			<a class="menu-logo grid4" href="<?php if (is_front_page()){echo "#";}else{echo get_home_url();}?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="Brafé"></a>	
            	<nav class="menu-nav grid8">
					<ul>	
						<li>
							<a <?php
				 			$args = array(
							'menu' => 'principal',
							'container' => false 
							);
						wp_nav_menu( $args ) ?> > 
							</a> 
						</li>
					</ul>
			</nav>
		</div>
	</header>