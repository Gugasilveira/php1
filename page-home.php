<?php
// Template Name: Home Page

?>



<?php get_header(); ?>

<main class="inicio">
		<h1><?php the_field('titulo_chamativo') ?></h1>
		<p><?php the_field('subtitulo_chamativo') ?></p>
	</main>
	
	<section class="sobre" id="sobre">
		<h2><?php the_field('uma_mistura_de') ?></h2>
		<div class="container">
			<div class="sobre-item grid6">
				<img src="<?php the_field('cafe_amor') ?>">
				<h3><?php the_field('amor') ?></h3>
			</div>
			<div class="sobre-item grid6">
				<img src="<?php the_field('cafe_perfeicao') ?>">
				<h3><?php the_field('perfeicao') ?></h3>
			</div>
		</div>
		<p><?php the_field('bio') ?></p>
	</section>
	
	<section class="produtos" id="produtos">
		<div class="container">
			<div class="produtos-item grid4">
				<h2 class="produtos-paulista"><?php the_field('paulista') ?></h2>
				<p><?php the_field('info_produtos') ?></p>
			</div>
			<div class="produtos-item grid4">
				<h2 class="produtos-carioca"><?php the_field('carioca') ?></h2>
				<p><?php the_field('info_produtos') ?></p>
			</div>
			<div class="produtos-item grid4">
				<h2 class="produtos-mineiro"><?php the_field('mineiro') ?></h2>
				<p><?php the_field('info_produtos') ?></p>
			</div>
		</div>
		<a class="produtos-btn" href="https://www.youtube.com/watch?v=nPArs8lCyRs"><?php the_field('saiba_mais') ?></a>
	</section>
	
	<section class="unidades" id="unidades">
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('imagem_botafogo') ?>" alt="Brafé  Botafogo">
			</div>
			<div class="grid6">
				<h2><?php the_field('botafogo') ?></h2>
				<p><?php the_field('info_local') ?></p>
				<a href="https://www.google.com/maps/place/Botafogo+Praia+Shopping/@-22.9475571,-43.1831875,15z/data=!4m5!3m4!1s0x0:0xe110541cdcfacbbb!8m2!3d-22.9475571!4d-43.1831875 "><?php the_field('ver_mapa') ?></a>
			</div>
		</div>
		
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('imagem_iguatemi') ?>" alt="Brafé  Iguatemi">
			</div>
			<div class="grid6">
				<h2><?php the_field('iguatemi') ?></h2>
				<p><?php the_field('info_local') ?></p>
				<a href="https://www.google.com/maps/place/Iguatemi+S%C3%A3o+Paulo/@-23.577085,-46.6883649,15z/data=!4m5!3m4!1s0x0:0x5ffc6f2485c8ae!8m2!3d-23.577085!4d-46.6883649 "><?php the_field('ver_mapa') ?></a>
			</div>
		</div>
		
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php the_field('imagem_mineirao') ?>" alt="Brafé  Mineirão">
			</div>
			<div class="grid6">
				<h2><?php the_field('mineirao') ?></h2>
				<p><?php the_field('info_local') ?></p>
				<a href="https://www.google.com/search?q=shopping%20perto%20do%20mineirao&rlz=1C1CHBD_pt-PTBR886BR886&oq=shopping+perto+do+mineirao&aqs=chrome..69i57.4514j0j7&sourceid=chrome&ie=UTF-8&sxsrf=ALeKk02w5J-gFhlMoI7ADX_DVHoZ0bWp_g:1596402914503&npsic=0&rflfq=1&rlha=0&rllag=-19878874,-43976357,1561&tbm=lcl&rldimm=4721665169345495925&lqi=ChpzaG9wcGluZyBwZXJ0byBkbyBtaW5laXJhb1omCghzaG9wcGluZyIac2hvcHBpbmcgcGVydG8gZG8gbWluZWlyYW8&phdesc=-pMRU8zliPE&ved=2ahUKEwjpqoyluP3qAhWgIbkGHWZAAEQQvS4wAHoECAwQIg&rldoc=1&tbs=lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:10&rlst=f#rlfi=hd:;si:4721665169345495925,l,ChpzaG9wcGluZyBwZXJ0byBkbyBtaW5laXJhb1omCghzaG9wcGluZyIac2hvcHBpbmcgcGVydG8gZG8gbWluZWlyYW8,y,-pMRU8zliPE;mv:[[-19.823936099999997,-43.947739399999996],[-19.898852599999998,-43.987939999999995]];tbs:lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:10 "><?php the_field('ver_mapa') ?></a>
			</div>
		</div>
	
	</section>
	
	<section class="contato" id="contato">
		<div class="container">
			<div class="contato-info grid6">
				<h2>Assine Nossa Newsletter</h2>
				<p>promoções e eventos mensais</p>
			</div>
			<form class="grid6">
				<label>E-mail</label>
				<input type="text" placeholder="Digite seu e-mail">
				<button type="submit">Enviar</button>
			</form>
		</div>
	</section>
	
	<?php get_footer(); ?>